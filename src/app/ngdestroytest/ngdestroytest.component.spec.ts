import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgdestroytestComponent } from './ngdestroytest.component';

describe('NgdestroytestComponent', () => {
  let component: NgdestroytestComponent;
  let fixture: ComponentFixture<NgdestroytestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgdestroytestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgdestroytestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
