import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {
  @Input() data:string='';
  @Output() events=new EventEmitter<String>();
  sendmessage()
  {
    this.events.emit('Hello From child component');
  }
  constructor() { }

  ngOnInit(): void {
  }

}
